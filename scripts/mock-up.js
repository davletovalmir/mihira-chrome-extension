let mockup = {};

mockup.contacts = [
  {first_name: 'Alisia', last_name: 'Heiser', phone_number: '0', type: 'home'},
  {first_name: 'Benjamin', last_name: 'WithLongLastName', phone_number: '1', type: 'mobile'},
  {first_name: 'Jessica', last_name: 'Johnson', phone_number: '+1 (234) 567-8900', type: 'California'},
  {first_name: 'Joseph', last_name: 'Tyler', phone_number: '+1 (234) 567-8900', type: 'Los Angeles, CA'},
  {first_name: 'Bradd', last_name: 'Pitt', phone_number: '+1 (234) 567-8900', type: 'pager'},
  {first_name: 'Alisia', last_name: 'Heiser', phone_number: '+1 (234) 567-8900', type: 'mobile'}
];

mockup.recentCalls = [
  {first_name: 'Alisia', last_name: 'Heiser', phone_number: '+1 (234) 567-8900', time: '12:13pm', type: 'mobile', status: 'incoming', duration: '00:07'},
  {first_name: 'Alisia', last_name: 'Heiser', phone_number: '+1 (234) 567-8900', time: '12:13pm', type: 'home', status: 'missing', duration: '00:07'},
  {first_name: 'Alisia', last_name: 'Heiser', phone_number: '+1 (234) 567-8900', time: '12:13pm', type: 'MD, USA', status: 'incoming', duration: '00:07'},
  {first_name: '', last_name: '', phone_number: '+1 (234) 567-8900', time: '12:13pm', type: 'Miami, FL', status: 'outgoing', duration: '00:07'}
];

mockup.voicemails = [
  {first_name: 'Alisia', last_name: 'Heiser', phone_number: '+1 (234) 567-8900', time: '12:13pm', datetime: 'Dec 14, 2016 at 5:36am', type: 'mobile', duration: '0:22'},
  {first_name: 'Alisia', last_name: 'Heiser', phone_number: '+1 (234) 567-8900', time: '12:13pm', datetime: 'Dec 14, 2016 at 5:36am', type: 'home', duration: '0:22'},
  {first_name: 'Alisia', last_name: 'Heiser', phone_number: '+1 (234) 567-8900', time: '12:13pm', datetime: 'Dec 14, 2016 at 5:36am', type: 'Moscow, Russia', duration: '0:22'},
  {first_name: 'Alisia', last_name: 'Heiser', phone_number: '+1 (234) 567-8900', time: '12:13pm', datetime: 'Dec 14, 2016 at 5:36am', type: 'New York, NY', duration: '0:22'},
  {first_name: 'Alisia', last_name: 'Heiser', phone_number: '+1 (234) 567-8900', time: '12:13pm', datetime: 'Dec 14, 2016 at 5:36am', type: 'Mars, Solar System', duration: '0:22'},
  {first_name: 'Alisia', last_name: 'Heiser', phone_number: '+1 (234) 567-8900', time: '12:13pm', datetime: 'Dec 14, 2016 at 5:36am', type: 'Miami, FL', duration: '0:22'},
  {first_name: 'Alisia', last_name: 'Heiser', phone_number: '+1 (234) 567-8900', time: '12:13pm', datetime: 'Dec 14, 2016 at 5:36am', type: 'Los Angeles, CA', duration: '0:22'},
  {first_name: 'Alisia', last_name: 'Heiser', phone_number: '+1 (234) 567-8900', time: '12:13pm', datetime: 'Dec 14, 2016 at 5:36am', type: 'mobile', duration: '0:22'}
];
