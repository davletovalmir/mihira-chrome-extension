{
  window.DecoratorFactory = function(model, decorations) {
    const subscriber = {
      subscribe: (callback) => model.__callback = callback
    };

    const needObserve = (el) => typeof el === 'object' && el !== null && !el.__isProxy;

    const observe = (target, handler) => {
      Object.keys(target).forEach((key) => {
        const el = target[key];
        if (needObserve(el)) target[key] = observe(el, proxyHandlerForChildren);
      });

      return new Proxy(Object.assign(target, {__isProxy: true}), handler);
  	};

    const proxyHandlerForChildren = {
      set: (target, key, value) => {
        if (needObserve(value)) target[key] = observe(value, proxyHandlerForChildren);
        else target[key] = value;
        if (model.__callback) model.__callback();
        return true;
      }
    };

    const proxyHandler = Object.assign({}, proxyHandlerForChildren, {
      get: (target, key, _proxy) => (key in decorations) ? decorations[key] : target[key]
    });

    return observe(Object.assign(model, subscriber), proxyHandler);
  };
}
