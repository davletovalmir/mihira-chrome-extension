$(document).ready(() => {
  initRivets();

  /* This will invoke factories and get Model.SomeModel from Model.AppFactory */
  Object.keys(Model).forEach((factoryName) => {
    const modelName = factoryName.replace('Factory', '');
    Model[modelName] = Model[factoryName]();
  });

  /* This will invoke factories and get Model.SomeModel from Model.AppFactory */
  Object.keys(Decorator).forEach((modelName) => {
    const [decorator, model] = [Decorator[modelName], Model[modelName]];
    Model[modelName] = decorator(model);
  });

  rivets.bind($(document.body), {
    app: Model.App,
    dialer: Model.Dialer,
    recents: Model.RecentCall,
    contacts: Model.Contact,
    voicemails: Model.Voicemail,
    tabs: Model.Tab,
    keypad: Model.Keypad,
    caller: Model.Caller,
    incomingCall: Model.IncomingCall,
    handlers: {
      contact: {
        sortListByName: (e) => {
          let name = $(e.currentTarget).val();
          Model.Contact.sortListByName(name);
        },
        showModal: (contact) => {
          return (e) => {
            Model.Contact.showModal(contact);
          }
        },
        confirmSelecting: (contact) => {
          return (e) => {
            Model.Contact.setSelected(contact);
            Model.Contact.hideModal();
          }
        },
        cancelSelecting: (e) => {
          Model.Contact.setSelected(null);
          Model.Contact.hideModal();
        }
      },
      dialer: {
        show: (e) => {
          Model.Dialer.show();
        },
        hide: (e) => {
          Model.Dialer.hide();
        }
      },
      tab: {
        switch: (e) => {
          const tabId = e.currentTarget.getAttribute('data-tab-id');
          Model.Tab.switchTo(tabId);
          Model.Voicemail.stop();
        }
      },
      keypad: {
        addDigit: (e) => {
          Model.Keypad.input += e.currentTarget.innerHTML;
        },
        removeDigit: (e) => {
          let digits = Model.Keypad.input;
          Model.Keypad.input = digits.slice(0, digits.length - 1);
        }
      },
      caller: {
        startCall: (e) => {
          Model.Caller.startCall(Model.Keypad.input);
        },
        finishCall: (e) => {
          Model.Caller.finishCall();
        },
        holdFirstCallee: (e) => {
          Model.Caller.toggleHold("first");
        },
        holdSecondCallee: (e) => {
          Model.Caller.toggleHold("second");
        },
        showKeypad: (e) => {
          Model.Caller.showKeypad();
        },
        hideKeypad: (e) => {
          Model.Caller.hideKeypad();
        },
        addCalleeKeypad: (e) => {
          Model.Caller.showKeypad({forConsultTransfer: true});
        },
        swapCallees: (e) => {
          Model.Caller.swapCallees();
        },
        mergeCall: (e) => {
          Model.Caller.mergeCall();
        },
        splitCall: (e) => {
          Model.Caller.splitCall();
        },
        removeFirstCallee: () => {
          Model.Caller.removeCallee('first');
        },
        removeSecondCallee: () => {
          Model.Caller.removeCallee('second');
        },
        startMultipleCall: (e) => {
          Model.Caller.startMultipleCall();
        },
        startBlindTransfer: (e) => {
          const mockCallback = (phoneNumber) => console.log(`Blind transfer to: ${phoneNumber}`);
          Model.Caller.startBlindTransfer(mockCallback);
        },
        startConsultTransfer: (e) => {
          const mockCallback = (phoneNumber) => console.log(`Consult transfer to: ${phoneNumber}`);
          Model.Caller.startConsultTransfer(mockCallback);
        },
        blindTransferKeypad: (e) => {
          if (!Model.Caller.modes.isSingleCalling) return;
          Model.Caller.showKeypad({forBlindTransfer: true});
        },
        showFirstNameTooltip: (e) => {
          return () => {
            if (Model.Caller.callees.first.fullname.length > 23)
              Model.Caller.callees.first.tooltipClass = '';
          };
        },
        hideFirstNameTooltip: (e) => {
          return () => Model.Caller.callees.first.tooltipClass = 'hidden';
        },
        showSecondNameTooltip: (e) => {
          return () => {
            if (Model.Caller.callees.second.fullname.length > 23)
              Model.Caller.callees.second.tooltipClass = '';
          };
        },
        hideSecondNameTooltip: (e) => {
          return () => Model.Caller.callees.second.tooltipClass = 'hidden';
        }
      },
      incomingCall: {
        accept: () => {
          Model.Tab.switchTo('keypad');
          Model.IncomingCall.accept();
        },
        reject: () => {
          Model.IncomingCall.reject();
        },
        blindTransfer: () => {
          Model.Tab.switchTo('keypad');
          Model.IncomingCall.blindTransfer();
        }
      },
      voicemail: {
        switch: (e) => {
          Model.Voicemail.switchTo(e.currentTarget);
        },
        play: (e) => {
          Model.Voicemail.play();
        },
        pause: (e) => {
          Model.Voicemail.pause();
        }
      }
    }
  });

  setTimeout(() => Model.App.load(), 500);

  /* LEGACY CODE */

  $(selectors.reminderDate).flatpickr({
    allowInput: true
  });

  $(selectors.reminderTime).timepicker({
    step: 15,
    timeFormat: 'H:i'
  });

  $(selectors.setReminder).on('change', (e) => {
    let isChecked = $(e.currentTarget).prop('checked');
    $(selectors.reminderInputs).attr('disabled', !isChecked);
  });
});

const selectors = {
  setReminder: '.js-set-reminder',
  reminderInputs: '.js-reminder-date, .js-reminder-time',
  reminderDate: '.js-reminder-date',
  reminderTime: '.js-reminder-time'
}

function initRivets() {
  rivets.configure(RIVETS_CONFIG);

  rivets.binders.class = (el, dynamicClass) => {
    let $el = $(el),
        staticClass = $el.attr('static-class');

    if (staticClass === undefined) {
      staticClass = $el.attr('class') || '';
      $el.attr('static-class', staticClass);
    }

    const className = `${staticClass} ${dynamicClass}`.trim();
    $el.attr('class', className);
  };

  rivets.binders.mouseover = (el, callback) => {
    const $el = $(el);
    $el.on('mouseover', callback);
  };

  rivets.binders.mouseout = (el, callback) => {
    const $el = $(el);
    $el.on('mouseout', callback);
  };

  // rivets.binders.tooltip = (el, text) => {
  //   const $el = $(el);
  //   let tooltip;
  //
  //   if (!$el.data('tooltip')) {
  //     tooltip = new CalleeNameTooltip(el, text);
  //     $el.data('tooltip', tooltip);
  //   } else tooltip = $el.data('tooltip');
  //
  //   $el.off('mouseover', tooltip.show);
  //   $el.off('mouseout', tooltip.hide);
  //
  //   $el.on('mouseover', tooltip.show);
  //   $el.on('mouseout', tooltip.hide);
  // };

  rivets.formatters.isActive = (currentId, comparisonId) => {
    return (currentId === comparisonId) ? 'active' : '';
  };

  rivets.adapters[':'] = {
    observe: (obj, keypath, callback) => obj.subscribe(callback),
    get: (obj, keypath) => obj[keypath](),
    unobserve: () => {},
    set: () => {}
  };
}
