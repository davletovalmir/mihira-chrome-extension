{
  window.Model = window.Model || {};

  window.Model.KeypadFactory = function() {
    const keyRows = [].slice.call(KEYPAD_CONFIG);

    return {
      input: '',
      keyRows: keyRows
    };
  };
}
