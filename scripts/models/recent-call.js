{
  window.Model = window.Model || {};

  window.Model.RecentCallFactory = function() {
    let list = mockup.recentCalls.map((contact) => new RecentCall(contact));
    let state = {
      list: list,
      last: list[list.length - 1]
    };

    let methods = {
      add: function (contact, status) {
        let now = new Date(),
            hours = now.getHours(),
            minutes = now.getMinutes();
        hours = hours > 9 ? hours : `0${hours}`;
        minutes = minutes > 9 ? minutes : `0${minutes}`;

        let time = `${hours}:${minutes}`;

        const recentCall = new RecentCall(
          Object.assign({}, contact.json, {
            status: status,
            time: time
          })
        );

        this.list.push(recentCall);

        this.setLast();

        return recentCall;
      },
      setLast: function () {
        this.last = this.list[this.list.length - 1];
      }
    };

    return Object.assign({}, state, methods);

  };

  class RecentCall {
    constructor(contact) {
      this.contact = contact;
    }

    update(json) {
      Object.assign(this.contact, json);
    }

    get fullnameExists() {
      return `${this.contact.first_name} ${this.contact.last_name}`.trim() != '';
    }

    get json() {
      return this.contact;
    }
  }
}
