{
  window.Model = window.Model || {};

  window.Model.DialerFactory = function() {
    let state = {
      isActive: false,
      className: 'hidden'
    };

    let methods = {
      show: function () {
        this.isActive = true;
        this.toggleClassName();
      },
      hide: function () {
        this.isActive = false;
        this.toggleClassName();
      },
      toggleClassName: function () {
        if (this.isActive) this.className = '';
        else this.className = 'hidden';
      }
    };

    return Object.assign({}, state, methods);
  };
}
