{
  window.Model = window.Model || {};

  window.Model.ContactFactory = function() {
    const fullList = mockup.contacts.map((contact) => new Contact(contact));

    let state = {
      list: fullList,
      selected: null,
      selectedInModal: null,
      modalClassName: 'hidden'
    };

    let methods = {
      sortListByName(name) {
        this.list = this.findByName(name);
      },
      findByName(name) {
        return ContactSearchService.findByName(name, fullList);
      },
      findByNumber(phoneNumber) {
        return ContactSearchService.findByNumber(phoneNumber, fullList);
      },
      setSelected(contact) {
        this.selected = contact;
      },
      showModal(contact) {
        this.selectedInModal = contact;
        this.isModalActive = true;
        this.toggleModalClassName();
      },
      hideModal() {
        this.selectedInModal = null;
        this.isModalActive = false;
        this.toggleModalClassName();
      },
      toggleModalClassName() {
        if (this.isModalActive) this.modalClassName = '';
        else this.modalClassName = 'hidden';
      }
    };

    return Object.assign({}, state, methods);
  };

  class Contact {
    constructor(contact) {
      if (typeof contact === 'string' || typeof contact === 'number')
        this.contact = Object.assign({}, this._blankContact, { phone_number: contact.toString() });
      else this.contact = contact;
    }

    get firstName() {
      if (this.hasNoName) return this.phoneNumber;
      else return this.contact.first_name.trim();
    }

    get lastName() {
      if (this.hasNoName) return this.phoneNumber;
      else return this.contact.last_name.trim();
    }

    get phoneNumber() {
      return this.contact.phone_number;
    }

    get fullname() {
      if (this.hasNoName) return this.phoneNumber;
      else return `${this.firstName} ${this.lastName}`.trim();
    }

    get json() {
      return this.contact;
    }

    get hasNoName () {
      return this.contact.first_name.trim() === '' &&
             this.contact.last_name.trim() === '';
    }

    /* private */

    get _blankContact() {
      return {
        first_name: '',
        last_name: '',
        phone_number: '',
        type: 'mobile'
      };
    }
  }

  class ContactSearchService {
    static findByName (name, list) {
      return list.filter((contact, id) => {
        let sName = name.split(/\s/g),
            firstName = contact.json.first_name,
            lastName = contact.json.last_name;

        sName = sName.map((name) => name.length > 0 ? (name[0].toUpperCase() + name.slice(1)) : '');

        if (sName.length > 1)
          return (firstName.includes(sName[0]) && lastName.includes(sName[1]));
        else
          return (firstName.includes(sName[0]) || lastName.includes(sName[0]));
      });
    }

    static findByNumber (phoneNumber, list) {
      let matchingContacts = list.filter(
        (contact) => contact.phoneNumber === phoneNumber.toString()
      );
      return matchingContacts[0] || new Contact(phoneNumber);
    }
  }
}
