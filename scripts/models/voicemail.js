{
  window.Model = window.Model || {};

  window.Model.VoicemailFactory = function() {
    let state = {
      current: 0,
      list: mockup.voicemails,
      isPlaying: false,
      playBtnClass: '',
      pauseBtnClass: 'hidden'
    };

    let methods = {
      switchTo: function (el) {
        let voicemailId = parseInt(el.getAttribute('data-voicemail-id'));
        if (this.current === voicemailId) return;
        this.current = voicemailId;
        this.stop();
        this.togglePlayPauseClass();
      },
      play: function () {
        this.isPlaying = true;
        this.togglePlayPauseClass();
      },
      pause: function () {
        this.isPlaying = false;
        this.togglePlayPauseClass();
      },
      stop: function () {
        if (!this.isPlaying) false;
        this.pause();
      },
      togglePlayPauseClass: function () {
        [this.playBtnClass, this.pauseBtnClass] =
          this.isPlaying ? ['hidden', ''] : ['', 'hidden'];
      }
    };
    return Object.assign({}, state, methods);
  };
}
