{
  window.Model = window.Model || {};

  window.Model.TabFactory = function() {
    let config = Object.assign({}, TABS_CONFIG);
    config.list =
      config.list
        .map((tabId) => Object.assign(TABS_CONFIG[tabId], { id: tabId }))
        .filter((el) => el.display);

    let state = {
      current: config.default,
      sizeClass: `m${12 / config.list.length}`
    };

    let methods = {
      switchTo: function (tabId) {
        this.current = tabId;
      }
    };

    return Object.assign({}, config, state, methods);
  };
}
