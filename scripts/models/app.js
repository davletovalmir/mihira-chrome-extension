{
  window.Model = window.Model || {};

  window.Model.AppFactory = function() {
    let state = {
      loaded: false,
      coverClassName: ''
    };

    let methods = {
      hideCover: function () {
        this.coverClassName = 'hidden';
      },
      load: function () {
        this.loaded = true;
        this.hideCover();
      }
    };

    return Object.assign({}, state, methods);
  };
}
