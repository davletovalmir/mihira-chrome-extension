{
  window.Model = window.Model || {};

  window.Model.CallerFactory = function() {
    let state = {
      keypad: {
        isActive: false,
        forToneMode: false,
        forBlindTransfer: false,
        forConsultTransfer: false
      },
      modes: {
        isCalling: false,
        isSingleCalling: false,
        isMultipleCalling: false,
        isMergedCalling: false,
        isIncomingBlindTransfering: false
      },
      classNames: {
        mode: 'single hidden',
        callBtn: '',
        endCallBtn: 'hidden',
        blindTransferBtn: 'hidden',
        consultTransferBtn: 'hidden',
        startBlindTransferBtn: 'disabled',
        startConsultTransferBtn: 'disabled',
        hideKeypadBtn: 'hidden',
        holdBtn: ''
      },
      texts: {
        holdBtn: 'hold'
      },
      callees: {
        first: null,
        second: null,
        active: null
      },
      keypadInput: '',
      mergedPhoneNumber: '',
      recentCall: null
    };

    let methods = {
      startCall: function (phoneNumber = '') {
        if (phoneNumber.trim() === '') return;
        this._switchModeTo('isSingleCalling');
        this.keypadInput = Model.Keypad.input;
        Model.Keypad.input = '';

        this.addCallee(phoneNumber);
        this.setActiveCallee(this.callees.first);

        this.recentCall = Model.RecentCall.add(this.callees.first.contact, 'outgoing');

        this._toggleClassNames();
      },
      handleIncomingCall: function (phoneNumber) {
        this._switchModeTo('isSingleCalling');
        this.keypadInput = Model.Keypad.input;
        Model.Keypad.input = '';

        this.addCallee(phoneNumber);
        this.setActiveCallee(this.callees.first);

        this.recentCall = Model.RecentCall.add(this.callees.first.contact, 'incoming');

        this._toggleClassNames();
      },
      startMultipleCall: function () {
        this._switchModeTo('isMultipleCalling');
        this.addSecondCallee(Model.Keypad.input);
        Model.Keypad.input = '';
        this.hideKeypad();
      },
      establishConnection: function (order) {
        if (this.callees[order] === null) return;
        this.callees[order].establishConnection();
      },
      toggleHold: function (order) {
        let callee;
        if (order === undefined) {
          callee = this.callees.active;
        } else {
          callee = this.callees[order];
        }

        if (callee !== this.callees.active) return;

        if (callee.isOnHold) {
          callee.unhold();
          this.texts.holdBtn = 'hold';
          this.classNames.holdBtn = '';
        } else {
          callee.hold();
          this.texts.holdBtn = 'unhold';
          this.classNames.holdBtn = 'held';
        }
      },
      addCallee: function (phoneNumber) {
        if (this.callees.first) return;

        const contact = Model.Contact.findByNumber(phoneNumber);
        this.callees.first = new Callee(contact);
        // Pretend waiting for a call 1 second and then start call
        setTimeout(() => this.establishConnection('first'), 1000);
      },
      addSecondCallee: function (phoneNumber) {
        if (this.callees.first === null || this.modes.isMergedCalling) return;

        const contact = Model.Contact.findByNumber(phoneNumber || 123);
        this.callees.second = new Callee(contact);
        this.callees.first.hold();
        this.setActiveCallee(this.callees.second);
        this._switchModeTo('isMultipleCalling');
        this._toggleClassNames();
        // Pretend waiting for a call 1 second and then start call
        setTimeout(() => this.establishConnection('second'), 1000);
      },
      setActiveCallee: function (callee) {
        if (this.callees.active) this.callees.active.activeClass = '';
        this.callees.active = callee;
        this.callees.active.activeClass = 'active-callee';
      },
      swapCallees: function () {
        if (this.callees.second === null) return;

        if (this.callees.active === this.callees.first) {
          this.callees.first.hold();
          this.callees.second.unhold();
          this.setActiveCallee(this.callees.second);
        } else {
          this.callees.first.unhold();
          this.callees.second.hold();
          this.setActiveCallee(this.callees.first);
        }
      },
      finishCall: function () {
        Model.Keypad.input = this.keypadInput;

        if (this.callees.first) {
          this.recentCall.update({duration: this.callees.first.formattedDuration});
          this.callees.first.destroy();
        }
        if (this.callees.second) this.callees.second.destroy();

        this._resetState();
        this._toggleClassNames();
      },
      mergeCall: function () {
        this._switchModeTo('isMergedCalling');
        this._toggleClassNames();
      },
      splitCall: function () {
        this._switchModeTo('isMultipleCalling');
        this._toggleClassNames();
      },
      removeCallee: function (order) {
        const callee = this.callees[order];
        if (callee !== this.callees.active) return;
        callee.destroy();
        if (order === 'first') [this.callees.first, this.callees.second] = [this.callees.second, null];
        else this.callees.second = null;

        this.setActiveCallee(this.callees.first);
        this._switchModeTo('isSingleCalling');
        this._toggleClassNames();
      },
      startBlindTransfer: function (callback = () => {}) {
        if (!(this.modes.isSingleCalling || this.modes.isIncomingBlindTransfering)) return;
        callback(Model.Keypad.input);
        this.finishCall();
      },
      startConsultTransfer: function (callback = () => {}) {
        if (!(this.modes.isMultipleCalling || this.modes.isMergedCalling)) return;
        callback(this.callees.second.phoneNumber);
        this.finishCall();
      },
      showIncomingBlindTransferKeypad: function (callback = () => {}) {
        this._switchModeTo('isIncomingBlindTransfering');
        this.showKeypad({forBlindTransfer: true});
      },
      showKeypad: function (props = {forToneMode: true}) {
        this.keypad.isActive = true;
        if (props) Object.assign(this.keypad, props);
        this._toggleClassNames();
      },
      hideKeypad: function () {
        if (this.modes.isIncomingBlindTransfering) {
          this._resetState();
          Model.Keypad.input = ''
          Model.IncomingCall.show();
          Model.Dialer.hide();
        } else {
          Object.keys(this.keypad).forEach((prop) => this.keypad[prop] = false);
        }
        this._toggleClassNames();
      },
      _switchModeTo: function (modesToSwitch) {
        if (!Array.isArray(modesToSwitch)) modesToSwitch = [modesToSwitch];
        modesToSwitch.push('isCalling');
        Object.keys(this.modes).forEach((modeName) => {
          if (modesToSwitch.indexOf(modeName) > -1) this.modes[modeName] = true;
          else this.modes[modeName] = false;
        });
      },
      _toggleClassNames: function () {
        let modeClassName = this.classNames.mode;

        if (this.modes.isCalling)
          if (this.keypad.isActive) modeClassName += ' keypad-active';
          else if (this.modes.isMultipleCalling) modeClassName = 'multiple';
          else if (this.modes.isMergedCalling) modeClassName = 'merged';
          else if (this.modes.isIncomingBlindTransfer) modeClassName = 'incoming-blind-transfer';
          else modeClassName = 'single';
        else modeClassName = 'single hidden';

        this.classNames.mode = modeClassName;

        this.classNames.callBtn = this.modes.isCalling ? 'hidden' : '';
        this.classNames.endCallBtn = this.keypad.forToneMode ? '' : 'hidden';
        this.classNames.blindTransferBtn = this.keypad.forBlindTransfer ? '' : 'hidden';
        this.classNames.consultTransferBtn = this.keypad.forConsultTransfer ? '' : 'hidden';
        this.classNames.startBlindTransferBtn = this.modes.isSingleCalling ? '' : 'disabled';
        this.classNames.startConsultTransferBtn =
          this.modes.isMultipleCalling || this.modes.isMergedCalling
          ? '' : 'disabled';
        this.classNames.hideKeypadBtn = this.keypad.isActive ? '' : 'hidden';
      },
      _resetState: function () {
        Object.assign(this, cloneState());
      }
    };

    class Callee {
      constructor (contact) {
        this.isRinging = true;
        this.isTalking = false;
        this.isOnHold = false;
        this.contact = contact;
        this.duration = 0;
        this.formattedDuration = this._formattedDuration();
        this.durationIntervalId = null;
        this.onHoldClass = '';
        this.activeClass = '';
        this.tooltipClass = 'hidden';
      }

      establishConnection () {
        this.isRinging = false;
        this.isTalking = true;
        this._startTimer();
      }

      destroy () {
        this._stopTimer();
      }

      hold () {
        this.isOnHold = true;
        this.onHoldClass = 'on-hold';
      }

      unhold () {
        this.isOnHold = false;
        this.onHoldClass = '';
      }

      _startTimer () {
        this.durationIntervalId = setInterval(() => {
          this.duration += .1;
          this.formattedDuration = this._formattedDuration();
        }, 100);
      }

      _restartTimer () {
        this.stopTimer();
        this.duration = 0;
        this.startTimer();
      }

      _stopTimer () {
        clearInterval(this.durationIntervalId);
      }

      _formattedDuration () {
        if (this.isRinging) return '...';

        let seconds = Math.floor(this.duration % 60);
        let minutes = Math.floor(this.duration / 60);
        seconds = seconds > 9 ? seconds : `0${seconds}`;
        minutes = minutes > 9 ? minutes : `0${minutes}`;
        return `${minutes}:${seconds}`;
      }

      get firstName () {
        return this.contact.firstName;
      }

      get fullname () {
        return this.contact.fullname;
      }

      get phoneNumber () {
        return this.contact.phoneNumber;
      }
    }

    function cloneState () {
      return JSON.parse(JSON.stringify(state));
    }

    return Object.assign({}, cloneState(), methods);
  };
}
