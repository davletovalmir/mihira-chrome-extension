{
  window.Model = window.Model || {};

  window.Model.IncomingCallFactory = function() {
    let state = {
      className: 'hidden',
      modalClassName: 'hidden',
      callee: null,
      activateDialer: false
    };

    let methods = {
      activate: function (phoneNumber) {
        this.callee = Model.Contact.findByNumber(phoneNumber);

        if (Model.Caller.modes.isCalling) return this.reject();
        if (this.activateDialer = Model.Dialer.isActive) Model.Dialer.hide();

        this.show();
      },
      deactivate: function (activateDialer = this.activateDialer) {
        this.hide(activateDialer);
        this.callee = null;
      },
      show: function () {
        this.className = '';
      },
      hide: function (activateDialer = this.activateDialer) {
        if (activateDialer) Model.Dialer.show();
        this.className = 'hidden';
      },
      accept: function () {
        Model.Caller.handleIncomingCall(this.callee.phoneNumber);
        this.deactivate(true);
      },
      reject: function () {
        Model.RecentCall.add(this.callee, 'missing');
        this.deactivate();
      },
      blindTransfer: function (callback = () => {}) {
        Model.Caller.showIncomingBlindTransferKeypad();
        this.hide(true);
      }
    };

    return Object.assign({}, state, methods);
  };
}
