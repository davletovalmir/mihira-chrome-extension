const RIVETS_CONFIG = {
  prefix: 'bind',
  preloadData: true,
  rootInterface: '.',
  templateDelimiters: ['{', '}'],
  iterationAlias: (modelName) => `$index`,
  executeFunctions: true
};

const TABS_CONFIG = {
  default: 'keypad',
  list: ['contacts', 'recent', 'keypad', 'voicemail'],
  contacts: {
    name: 'Contacts',
    heading: 'All contacts',
    iconClass: 'icon-contacts',
    display: true
  },
  recent: {
    name: 'Recents',
    heading: 'Recent calls',
    iconClass: 'icon-recent',
    display: true
  },
  keypad: {
    name: 'Keypad',
    heading: '',
    iconClass: 'icon-keypad',
    display: true
  },
  voicemail: {
    name: 'Voicemail',
    heading: 'Voicemail',
    iconClass: 'icon-voicemail',
    display: true
  }
};

const KEYPAD_CONFIG = [
  ['1', '2', '3'],
  ['4', '5', '6'],
  ['7', '8', '9'],
  ['*', '0', '#']
];
