{
  window.Decorator = window.Decorator || {};

  window.Decorator.Caller = function(model) {
    var decorations = {
      isSingleOrMergedCall: () => {
        return model.modes.isSingleCalling || model.modes.isMergedCalling;
      },
      isMergedOrSplittedCall: () => {
        return model.modes.isSingleCalling || model.modes.isMergedCalling || model.modes.isSplitted;
      }
    };

    return window.DecoratorFactory(model, decorations);
  };
}
